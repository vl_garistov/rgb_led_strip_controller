/*
 * LED_strip.c
 *
 *  Created on: Dec 31, 2023
 *      Author: Vladimir Garistov
 */

/*
    RGB LED strip controller
    A controller for 3 RGB LED strips with some simple effects and brightness control.
    Copyright (C) 2023-2024  Vladimir Garistov <vl.garistov@gmail.com>, Boris Dundakov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LED_strip.h"
#include "stm32f1xx_ll_tim.h"
#include <stdbool.h>

static void set_brightness(LED_strip_channel_t channel, uint16_t red, uint16_t green, uint16_t blue);
static bool effect_fade(uint32_t t);
static bool effect_blink(uint32_t t);
static bool effect_levski(uint32_t t);
static bool effect_police(uint32_t t);

volatile uint16_t pot_val = 0;
static LED_strip_mode_t LED_strip_mode = POLICE;

void change_mode(void)
{
	switch (LED_strip_mode)
	{
		case SLOW_FADE: LED_strip_mode = LEVSKI; break;
		case LEVSKI: LED_strip_mode = BLINK; break;
		case BLINK: LED_strip_mode = POLICE; break;
		case POLICE: LED_strip_mode = SLOW_FADE; break;
		default:;
	}
}

void update_LED_strip(uint32_t current_time)
{
	static uint32_t start_time = 0;
	static LED_strip_mode_t old_mode = POLICE;

	if (LED_strip_mode != old_mode)
	{
		old_mode = LED_strip_mode;
		start_time = current_time;
	}

	uint32_t t_diff = current_time - start_time;
	bool effect_finished = false;

	switch (LED_strip_mode)
	{
		case SLOW_FADE:
			effect_finished = effect_fade(t_diff);
			break;
		case LEVSKI:
			effect_finished = effect_levski(t_diff);
			break;
		case BLINK:
			effect_finished = effect_blink(t_diff);
			break;
		case POLICE:
			effect_finished = effect_police(t_diff);
			break;
		default:;
	}
	if (effect_finished)
	{
		t_diff = 0;
		start_time = current_time + 1;
	}
}

static bool effect_police(uint32_t t)
{
	if (t < 10)
	{
		set_brightness(LED_STRIP_CH0, 1000, 0, 0);
		set_brightness(LED_STRIP_CH1, 0, 0, 0);
		set_brightness(LED_STRIP_CH2, 0, 0, 0);
		return false;
	}
	else if (t == 500)
	{
		set_brightness(LED_STRIP_CH0, 0, 0, 0);
		set_brightness(LED_STRIP_CH1, 0, 0, 0);
		set_brightness(LED_STRIP_CH2, 0, 0, 1000);
		return false;
	}
	else if (t == 999)
	{
		return true;
	}
	return true;
}

static bool effect_fade(uint32_t t)
{
	//RED FADE
	if (t < 10)
	{
		set_brightness(LED_STRIP_CH0, 1000, 0, 0);
		set_brightness(LED_STRIP_CH1, 1000, 0, 0);
		set_brightness(LED_STRIP_CH2, 1000, 0, 0);
		return false;
	}
	else if (t == 5000)
	{
		set_brightness(LED_STRIP_CH0, 800, 0, 0);
		set_brightness(LED_STRIP_CH1, 800, 0, 0);
		set_brightness(LED_STRIP_CH2, 800, 0, 0);
		return false;
	}
	else if (t == 10000)
	{
		set_brightness(LED_STRIP_CH0, 600, 0, 0);
		set_brightness(LED_STRIP_CH1, 600, 0, 0);
		set_brightness(LED_STRIP_CH2, 600, 0, 0);
		return false;
	}
	else if (t == 15000)
	{
		set_brightness(LED_STRIP_CH0, 400, 0, 0);
		set_brightness(LED_STRIP_CH1, 400, 0, 0);
		set_brightness(LED_STRIP_CH2, 400, 0, 0);
		return false;
	}
	// BLUE FADE
	else if (t == 20000)
	{
		set_brightness(LED_STRIP_CH0, 0, 1000, 0);
		set_brightness(LED_STRIP_CH1, 0, 1000, 0);
		set_brightness(LED_STRIP_CH2, 0, 1000, 0);
		return false;
	}
	else if (t == 25000)
	{
		set_brightness(LED_STRIP_CH0, 0, 800, 0);
		set_brightness(LED_STRIP_CH1, 0, 800, 0);
		set_brightness(LED_STRIP_CH2, 0, 800, 0);
		return false;
	}
	else if (t == 30000)
	{
		set_brightness(LED_STRIP_CH0, 0, 600, 0);
		set_brightness(LED_STRIP_CH1, 0, 600, 0);
		set_brightness(LED_STRIP_CH2, 0, 600, 0);
		return false;
	}
	else if (t == 35000)
	{
		set_brightness(LED_STRIP_CH0, 0, 400, 0);
		set_brightness(LED_STRIP_CH1, 0, 400, 0);
		set_brightness(LED_STRIP_CH2, 0, 400, 0);
		return false;
	}
	// GREEN FADE
	else if (t == 40000)
	{
		set_brightness(LED_STRIP_CH0, 0, 0, 1000);
		set_brightness(LED_STRIP_CH1, 0, 0, 1000);
		set_brightness(LED_STRIP_CH2, 0, 0, 1000);
		return false;
	}
	else if (t == 45000)
	{
		set_brightness(LED_STRIP_CH0, 0, 0, 800);
		set_brightness(LED_STRIP_CH1, 0, 0, 800);
		set_brightness(LED_STRIP_CH2, 0, 0, 800);
		return false;
	}
	else if (t == 50000)
	{
		set_brightness(LED_STRIP_CH0, 0, 0, 600);
		set_brightness(LED_STRIP_CH1, 0, 0, 600);
		set_brightness(LED_STRIP_CH2, 0, 0, 600);
		return false;
	}
	else if (t == 55000)
	{
		set_brightness(LED_STRIP_CH0, 0, 0, 400);
		set_brightness(LED_STRIP_CH1, 0, 0, 400);
		set_brightness(LED_STRIP_CH2, 0, 0, 400);
		return true;
	}
	return true;
}

static bool effect_blink(uint32_t t)
{
	if (t < 10)
	{
		set_brightness(LED_STRIP_CH0, 1000, 0, 0);
		set_brightness(LED_STRIP_CH1, 1000, 0, 0);
		set_brightness(LED_STRIP_CH2, 1000, 0, 0);
		return false;
	}
	else if (t == 2500)
	{
		set_brightness(LED_STRIP_CH0, 0, 1000, 0);
		set_brightness(LED_STRIP_CH1, 0, 1000, 0);
		set_brightness(LED_STRIP_CH2, 0, 1000, 0);
		return false;
	}
	else if (t == 5000)
	{
		set_brightness(LED_STRIP_CH0, 0, 0, 1000);
		set_brightness(LED_STRIP_CH1, 0, 0, 1000);
		set_brightness(LED_STRIP_CH2, 0, 0, 1000);
		return true;
	}
	return true;
}

static bool effect_levski(uint32_t t)
{
	if (t < 10)
	{
		set_brightness(LED_STRIP_CH0, 0, 0, 1000);
		set_brightness(LED_STRIP_CH1, 0, 0, 1000);
		set_brightness(LED_STRIP_CH2, 0, 0, 1000);
		return false;
	}
	else if (t == 100000)
	{
		set_brightness(LED_STRIP_CH0, 0, 0, 1000);
		set_brightness(LED_STRIP_CH1, 0, 0, 1000);
		set_brightness(LED_STRIP_CH2, 0, 0, 1000);
		return true;
	}
	return true;

}

static void set_brightness(LED_strip_channel_t channel, uint16_t red, uint16_t green, uint16_t blue)
{
	// The channels in the switch's cases are the different strips and the timer channels
	// are the different colors.
	switch (channel)
	{
		case LED_STRIP_CH0:
			LL_TIM_OC_SetCompareCH1(TIM2, red);
			LL_TIM_OC_SetCompareCH2(TIM2, green);
			LL_TIM_OC_SetCompareCH3(TIM2, blue);
			break;
		case LED_STRIP_CH1:
			LL_TIM_OC_SetCompareCH1(TIM3, red);
			LL_TIM_OC_SetCompareCH2(TIM3, green);
			LL_TIM_OC_SetCompareCH3(TIM3, blue);
			break;
		case LED_STRIP_CH2:
			LL_TIM_OC_SetCompareCH1(TIM4, red);
			LL_TIM_OC_SetCompareCH2(TIM4, green);
			LL_TIM_OC_SetCompareCH3(TIM4, blue);
			break;
		default:;
	}
}
