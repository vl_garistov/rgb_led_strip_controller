/*
 * LED_strip.h
 *
 *  Created on: Dec 31, 2023
 *      Author: Vladimir Garistov
 */

/*
    RGB LED strip controller
    A controller for 3 RGB LED strips with some simple effects and brightness control.
    Copyright (C) 2023-2024  Vladimir Garistov <vl.garistov@gmail.com>, Boris Dundakov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INC_LED_STRIP_H_
#define INC_LED_STRIP_H_

#include <stdint.h>

typedef enum
{
	SLOW_FADE,
	LEVSKI,
	BLINK,
	POLICE
}
LED_strip_mode_t;

typedef enum
{
	LED_STRIP_CH0,	// 2m
	LED_STRIP_CH1,	// 1m
	LED_STRIP_CH2	// 2m
}
LED_strip_channel_t;

void change_mode(void);
void update_LED_strip(uint32_t current_time);

#endif /* INC_LED_STRIP_H_ */
